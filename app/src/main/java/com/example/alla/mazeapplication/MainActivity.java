package com.example.alla.mazeapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends ActionBarActivity {
    TextView tvTime;
    private static final String TAG = "MainActivity";
    private List<Vertex> vertexes;
    private List<String> visited;
    private List<List<Integer>> matrix;
    private int N, shortestWay = 10000;
    private AtomicInteger queryCount;
    private static List<Integer> path;
    private Set<String> queryMap;
    private double timeout = 0;
    private String mazeName = "easy", answer = "";
    private Button btnSolve, btnSend, btnSave, btnRetrieve;
    EditText etMazeName;

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vertexes = new ArrayList<Vertex>();
        visited = new CopyOnWriteArrayList<String>();
        path = new ArrayList<Integer>();

        tvTime = (TextView) findViewById(R.id.tv_time);

        btnSolve = (Button) findViewById(R.id.btnSolve);
        btnSend = (Button) findViewById(R.id.btnSend);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnRetrieve = (Button) findViewById(R.id.btnRetrieve);

        btnSolve.setEnabled(false);
        btnSend.setEnabled(false);
        btnSave.setEnabled(false);
        btnRetrieve.setEnabled(AppUtils.readSP(this));

        etMazeName = (EditText) findViewById(R.id.et_mazeName);
    }

    public void onclick(View v) {
        switch (v.getId()) {
            case R.id.btnDownload:
                downloadGraf();
                break;

            case R.id.btnSolve:
                N = vertexes.size();
                convertToMatrix();

                if (calculateNumberOfPowerPills() == 0) {
                    Log.d("MyLOG","+++");
                    easySolve();
                } else {
                    hardSolve();
                }

                btnSend.setEnabled(true);
                break;

            case R.id.btnSend:
                Intent intent = new Intent(this, SendSolutionActivity.class);
                intent.putExtra("mazeName", mazeName);
                intent.putExtra("path", answer);
                startActivity(intent);

                break;
            case R.id.btnSave:
                AppUtils.serializeQuotes(vertexes, this, mazeName);
                AppUtils.writeSP(this, true);
                btnRetrieve.setEnabled(true);
                break;

            case R.id.btnRetrieve:
                retrieveInformationFromFile();
                break;

            default:
                break;
        }
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();

            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    public Vertex parseJSON(String result) throws JSONException {
        JSONObject jObject = new JSONObject(result);
        if (jObject.has("MessageType")) {
            String messageType = jObject.getString("MessageType");
            if (messageType.equals("Error")) {
                Log.e(TAG, "Requested location does not exist");
                return null;
            }
        }

        String id = jObject.getString("LocationId");
        String type = jObject.getString("LocationType");
        List<String> exits = new ArrayList<>();
        JSONArray jArray = jObject.getJSONArray("Exits");
        for (int i = 0; i < jArray.length(); i++) {
            exits.add(jArray.getString(i));
        }

        Vertex vertex = new Vertex(id, type, exits);
        vertexes.add(vertex);
        if (type.equals("Start")) {
            visited.add(id);
        }
        return vertex;
    }

    public void convertToMatrix() {
        matrix = new ArrayList<>();
        for (Vertex vertex : vertexes) {
            int i = getNumberOfVertixById(vertex.getId());
            if(i%100 == 0) Log.d("MyLOG", "+" + i + "");
            matrix.add(new ArrayList<Integer>());
            List<String> temp = vertex.getExits();
            for (String item : temp) {
                int j = getNumberOfVertixById(item);
                matrix.get(i).add(j);
            }
            Log.d("MyLOG", i + ": " + matrix.get(i));
        }
    }

    private int type = 0;

    private void easySolve() {
        final String[] mChooseAlgo = {"Алгоритм Дейкстры", "Поиск в глубину"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        type = 0;
        builder.setTitle("Выберите алгоритм:")
                .setCancelable(false)
                .setNeutralButton("Ок",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                timeout = System.nanoTime();
                                if (type == 0) {
                                    path.clear();
                                    DijkstraAlgo dijkstraAlgo = new DijkstraAlgo(N, matrix);
                                    dijkstraAlgo.solve(0, N - 1);
                                    path = dijkstraAlgo.getAnswer();

                                    answer = "";
                                    for (int i = path.size() - 1; i >= 0; i--) {
                                        answer = answer + vertexes.get(path.get(i)).getId();
                                        if (i != 0) answer += ",";
                                    }
                                } else {
                                    path.clear();
                                    BFSAlgo bfsAlgo = new BFSAlgo(N, matrix);
                                    bfsAlgo.solve(0, N - 1);
                                    path = bfsAlgo.getAnswer();

                                    answer = "";
                                    for (int i = path.size() - 1; i >= 0; i--) {
                                        Log.d("MyLOG", i +  "=" + path.get(i));
                                        answer = answer + vertexes.get(path.get(i)).getId();
                                        if (i != 0) answer += ",";
                                    }
                                }
                                setTime();
                            }
                        })
                .setSingleChoiceItems(mChooseAlgo, 0,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                type = item;
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void hardSolve() {

        timeout = System.nanoTime();
        shortestWay = 10000;

        int numberOfNeedToVisitVertexes = calculateNumberOfPowerPills();

        Log.d("MyLOG", numberOfNeedToVisitVertexes + "+");
        int[] powerPills = new int[numberOfNeedToVisitVertexes];
        int ch = 0;
        for (int i = 0; i < N; i++) {
            if (vertexes.get(i).getType().equals("PowerPill")) {
                powerPills[ch] = i;
                ch++;
            }
        }

        long numberOfPermutation = AppUtils.factorial(numberOfNeedToVisitVertexes);

        for (int i = 0; i < 1; i++) {
            List<Integer> temp = new ArrayList<Integer>();
            temp.add(0);
            for (int index = 0; index < ch; index++) {
                temp.add(powerPills[index]);
            }
            temp.add(getNumberOfExit());

            path.clear();
            for (int j = 0; j < temp.size() - 1; j++) {
                BFSAlgo bfsAlgo = new BFSAlgo(N, matrix);
                bfsAlgo.solve(temp.get(j), temp.get(j + 1));
                List<Integer> y = path;
                if (y.size() != 0) {
                    y.remove(0);
                }

                path = bfsAlgo.getAnswer();
                path.addAll(y);
            }

            if (path.size() < shortestWay) {
                shortestWay = path.size();
                answer = "";
                for (int index = path.size() - 1; index >= 0; index--) {
                    answer = answer + vertexes.get(path.get(index)).getId();
                    if (index != 0) answer += ",";
                }
            }
            AppUtils.nextPermutation(powerPills);
        }
        setTime();
    }

    private int getNumberOfExit() {
        for (int i = 0; i < N; i++) {
            if (vertexes.get(i).getType().equals("Exit")) {
                return i;
            }
        }
        return (N - 1);
    }

    private void setTime() {
        timeout = System.nanoTime() - timeout;
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);
        tvTime.setText(getString(R.string.time) + " " + df.format(timeout / 1000000) + " мс");
    }

    private void downloadGraf() {
        mazeName = etMazeName.getText().toString();
        pd = new ProgressDialog(this);
        pd.setTitle("Подождите");
        pd.setMessage("Идет загрузка графа");
        pd.setCancelable(false);
        pd.show();
        queryMap = new ConcurrentSkipListSet<String>();
        vertexes.clear();
        visited.clear();
        queryCount = new AtomicInteger(0);
        new HttpAsyncTask().execute("http://labyrinth.digitaslbi.com/Maze/Location/" + mazeName + "/start/json");
    }

    private void retrieveInformationFromFile() {
        mazeName = etMazeName.getText().toString();
        vertexes = AppUtils.deserializeQuotes(this, mazeName);
        N = vertexes.size();
        if (N != 0) {
            Log.d("MyLOG", "All = " + N);
            btnSolve.setEnabled(true);
        }
    }

    private int calculateNumberOfPowerPills() {
        int needVisite = 0;
        for (int i = 0; i < N; i++) {
            Log.d("MyLOG", "+" + i + "");
            if (vertexes.get(i).getType().equals("PowerPill")) {
                needVisite++;
            }
        }
        return needVisite;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private int getNumberOfVertixById(String id) {
        for (int i = 0; i < N; i++) {
            if (id.contains(vertexes.get(i).getId())) {
                return i;
            }
        }
        return 0;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, Vertex> {
        @Override
        protected Vertex doInBackground(String... urls) {

            String result = GET(urls[0]);

            if (!result.equals("Did not work!")) {
                try {
                    return parseJSON(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Vertex vertex) {
            queryCount.getAndDecrement();

            if (vertex == null) {
                pd.dismiss();
                Toast.makeText(getBaseContext(), "Ошибка при загрузке данных!", Toast.LENGTH_SHORT).show();
                return;
            }

            if(queryMap.contains(vertex.getId())) {
                queryMap.remove(vertex.getId());
            }

            //Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_SHORT).show();

            List<String> temp = vertex.getExits();

            for (String item : temp) {
                String[] vertix_id = item.split("/");

                boolean vertix_not_visit = !visited.contains(vertix_id[vertix_id.length-1]);
                /*for (String visite : visited) {
                    if (item.contains(visite)) {
                        vertix_not_visit = false;
                        break;
                    }
                }*/

                if (vertix_not_visit) {
                    visited.add(vertix_id[vertix_id.length - 1]);
                    queryMap.add(vertix_id[vertix_id.length - 1]);
                    //new HttpAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "http://labyrinth.digitaslbi.com" + item + "/json");
                    new HttpAsyncTask().execute("http://labyrinth.digitaslbi.com" + item + "/json");
                }
            }
            if (queryMap.isEmpty()) {
                pd.dismiss();
                Toast.makeText(getBaseContext(), "Граф загружен!", Toast.LENGTH_SHORT).show();
                btnSolve.setEnabled(true);
                btnSave.setEnabled(true);
                return;
            } else {
                Log.d("MyLOG", "Size = " + vertexes.size());
            }
        }
    }
}