package com.example.alla.mazeapplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alla on 1/22/15.
 */
public class DijkstraAlgo {

    private int N;
    private List<List<Integer>> matrix;
    private List<Integer> answer;

    public DijkstraAlgo(int n, List<List<Integer>> matrix) {
        this.N = n;
        this.matrix = matrix;
    }

    public List<Integer> getAnswer() {
        return answer;
    }

    public void solve(int v, int f) {
        int[] d = new int[N];
        int[] c = new int[N];
        boolean[] b = new boolean[N];
        for (int i = 0; i < N; i++) {
            d[i] = 1000000;
            b[i] = false;
            c[i] = -1;
        }
        int g = v;
        d[g] = 0;
        while (g != -1) {
            List<Integer> temp = matrix.get(v);
            for (int i = 0; i < temp.size(); i++) {
                if (d[temp.get(i)] > d[g] + 1) {
                    d[temp.get(i)] = d[g] + 1;
                    c[temp.get(i)] = g;
                }
            }
            b[g] = true;
            g = -1;
            int min = 1000000;
            for (int j = 0; j < N; j++) {
                if (d[j] < min && b[j] != true) {
                    min = d[j];
                    g = j;
                }
            }
        }
        answer = new ArrayList<Integer>();
        answer.add(f+1);
        int i = f - 1;
        while (i > 0) {
            answer.add(c[i] + 1);
            i = c[i];
        }
    }
}
