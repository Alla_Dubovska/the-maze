package com.example.alla.mazeapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alla on 1/22/15.
 */
public class AppUtils {

    public static void serializeQuotes(List<Vertex> vertexes, Context context, String fileName) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName + ".txt", Context.MODE_WORLD_READABLE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(vertexes);
            oos.close();
            Toast.makeText(context, "Данные сохранены!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Vertex> deserializeQuotes(Context context, String fileName) {
        List<Vertex> vertexes = new ArrayList<Vertex>();
        try {
            FileInputStream fis = context.openFileInput(fileName + ".txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            vertexes = (ArrayList<Vertex>) ois.readObject();
            ois.close();
            Toast.makeText(context, "Данные извлечены!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(context, "Ошибка при загрузке данных!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return vertexes;
    }

    public static boolean readSP(Context context) {
        SharedPreferences sPref = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        return sPref.getBoolean(Constants.APP_PREFERENCES_DATA_SAVED, false);
    }

    public static void writeSP(Context context, boolean saved) {
        SharedPreferences sPref = context.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sPref.edit();
        editor.putBoolean(Constants.APP_PREFERENCES_DATA_SAVED, saved);
        editor.apply();
    }

    public static boolean nextPermutation(int[] a) {
        int N = a.length;
        int i = N - 2;
        for (; i >= 0; i--) if (a[i] < a[i + 1]) break;
        if (i < 0) return false;

        for (int j = N - 1; j >= i; j--) {
            if (a[j] > a[i]) {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                break;
            }
        }
        for (int j = i + 1; j < (N + i + 1) / 2; j++)        //reverse from a[i+1] to a[N-1]
        {
            int temp = a[j];
            a[j] = a[N + i - j];
            a[N + i - j] = temp;
        }
        return true;
    }

    public static long factorial(int n) {
        if (n == 0) return 1;
        return n * factorial(n - 1);
    }
}