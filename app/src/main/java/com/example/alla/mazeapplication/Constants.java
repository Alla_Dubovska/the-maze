package com.example.alla.mazeapplication;

/**
 * Created by alla on 1/22/15.
 */
public class Constants {
    public static final String APP_PREFERENCES = "mySettings";
    public static final String APP_PREFERENCES_DATA_SAVED = "dataSaved";
}
