package com.example.alla.mazeapplication;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by alla on 1/22/15.
 */
public class BFSAlgo {

    private int N;
    private List<List<Integer>> matrix;
    private List<Integer> answer;

    public BFSAlgo(int n, List<List<Integer>> matrix) {
        N = n;
        this.matrix = matrix;
    }

    public List<Integer> getAnswer() {
        return answer;
    }

    public void solve(int start, int finish) {

        Queue<Integer> queue = new LinkedList<Integer>();

        queue.add(start);

        boolean[] checked = new boolean[N];
        for (int i = 0; i < N; i++) {
            checked[i] = false;
        }

        int[] d = new int[N];
        int[] p = new int[N];

        checked[start] = true;
        p[start] = -1;

        while (!queue.isEmpty()) {
            int v = queue.remove();
            List<Integer> temp = matrix.get(v);
            for(int i = 0; i<temp.size(); i++) {
                if(!checked[temp.get(i)]) {
                    checked[temp.get(i)] = true;
                    queue.add(temp.get(i));
                    d[temp.get(i)] = d[v] + 1;
                    p[temp.get(i)] = v;
                }
            }

        }

        answer = new ArrayList<Integer>();
        for(int i = finish; i!=-1; i = p[i]) {
            answer.add(i);
        }
    }
}
