package com.example.alla.mazeapplication;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alla on 1/19/15.
 */
public class Vertex implements Serializable {
    private List<String> exits;
    private String id;
    private String type;

    public Vertex(String id, String type, List<String> exits) {
        this.id = id;
        this.type = type;
        this.exits = exits;
    }

    public String getType() {
        return type;
    }

    public String getId() {

        return id;
    }

    public List<String> getExits() {
        return exits;
    }
}
