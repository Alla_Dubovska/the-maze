package com.example.alla.mazeapplication;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public class SendSolutionActivity extends ActionBarActivity {

    private static final String TAG = "SendSolutionActivity";

    private EditText etName;
    private EditText etEmail;
    private EditText etLang;
    private EditText etMazeName;
    private EditText etPath;
    String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_solution);

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etLang = (EditText) findViewById(R.id.etLang);
        etMazeName = (EditText) findViewById(R.id.etMazeName);
        etPath = (EditText) findViewById(R.id.etPath);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            etMazeName.setText(extras.getString("mazeName"));
            path = extras.getString("path");
        }

        etPath.setText(path);

        Button btnSend = (Button) findViewById(R.id.button);
        View.OnClickListener oclBtnOk = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkEmail(etEmail.getText().toString()) && !isEmpty(etName) && !isEmpty(etEmail)
                        && !isEmpty(etLang) && !isEmpty(etMazeName) && !isEmpty(etPath)) {
                    new HttpPostAsyncTask().execute("http://labyrinth.digitaslbi.com//Maze/SubmitHighScore/json");
                } else {
                    Toast.makeText(SendSolutionActivity.this, "Проверьте корректность введеных данных!", Toast.LENGTH_SHORT).show();
                }

            }
        };
        btnSend.setOnClickListener(oclBtnOk);
    }


    public String POST(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            // 1. create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // 2. make POST request to the given URL
            HttpPost httpPost = new HttpPost(url);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
            nameValuePairs.add(new BasicNameValuePair("player_name", etName.getText().toString()));
            nameValuePairs.add(new BasicNameValuePair("email_address", etEmail.getText().toString()));
            nameValuePairs.add(new BasicNameValuePair("computer_language", etLang.getText().toString()));
            nameValuePairs.add(new BasicNameValuePair("maze_name", etMazeName.getText().toString()));
            nameValuePairs.add(new BasicNameValuePair("maze_path", path));

            // 6. set httpPost Entity
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // 8. Execute POST request to the given URL
            HttpResponse httpResponse = httpclient.execute(httpPost);

            String responseString = null;
            HttpEntity responseEntity = httpResponse.getEntity();
            if (responseEntity != null) {
                result = EntityUtils.toString(responseEntity);
            } else {
                result = "Did not work!";
            }

            // 9. receive response as inputStream
            //inputStream = httpResponse.getEntity().getContent();

        } catch (Exception e) {
            //Log.d("InputStream", e.getLocalizedMessage());
            Log.d("InputStream", e.getLocalizedMessage());
        }

        // 11. return result
        return result;
    }

    private static boolean checkEmail(String sEmail) {
        String sDomen = "[a-z][a-z[0-9]\u005F\u002E\u002D]*[a-z||0-9]";
        String sDomen2 = "([a-z]){2,4}";
        Pattern p = Pattern.compile(sDomen + "@" + sDomen + "\u002E" + sDomen2);
        Matcher m = p.matcher(sEmail.toLowerCase());
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    private class HttpPostAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return POST(urls[0]);
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "Data Sent");
            Toast.makeText(getBaseContext(), "Data Sent!", Toast.LENGTH_SHORT).show();
            TextView tvResult = (TextView) findViewById(R.id.tvResult);
            tvResult.setText(result);
        }
    }
}
